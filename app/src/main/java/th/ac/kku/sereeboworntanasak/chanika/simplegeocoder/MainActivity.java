package th.ac.kku.sereeboworntanasak.chanika.simplegeocoder;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RadioButton radio_addr, radio_lat_lng;
    Button bt;
    RadioGroup rg;
    EditText lat,lng,addr;
    TextView result;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
        if (id == R.id.simple_geocoder) {
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);

            return true;
        } else if(id == R.id.current_location){
            Intent intent = new Intent(this,CurrentLocationDisplay.class);
            startActivity(intent);

            return true;
        }else if(id == R.id.current_geocoder){
            Intent intent = new Intent(this,CurrentGeocoder.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lat = (EditText) findViewById(R.id.lat);
        lng = (EditText) findViewById(R.id.lng);
        addr = (EditText) findViewById(R.id.address);
        result = (TextView) findViewById(R.id.result);
        bt = (Button) findViewById(R.id.button);
        radio_lat_lng = (RadioButton) findViewById(R.id.radio1);
        radio_addr = (RadioButton) findViewById(R.id.radio2);


        bt.setOnClickListener(this);
        radio_addr.setOnClickListener(this);
        radio_lat_lng.setOnClickListener(this);

    }

    private void checkFunction() {
        if (radio_lat_lng.isChecked()) {

            lat.setEnabled(true);
            lng.setEnabled(true);
            addr.setEnabled(false);

            lat.requestFocus();
        } else if (radio_addr.isChecked()) {

            lat.setEnabled(false);
            lng.setEnabled(false);
            addr.setEnabled(true);

            addr.requestFocus();
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current ", "" + strReturnedAddress.toString());
            } else {
                Log.w("My Current ", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current ", "Canont get Address!");
        }
        return strAdd;
    }

    public void getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(strAddress, 5);


            Address location = address.get(0);

            Double lat = location.getLatitude();
            Double lng = location.getLongitude();

            Log.d("======", lat + " " + lng);
            result.setText("Lat:" + lat + " \nLng:" + lng);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        if (v == bt) {

            if (radio_lat_lng.isChecked()) {

                double double_lat = Double.parseDouble(lat.getText().toString());
                double double_lng = Double.parseDouble(lng.getText().toString());

                String resultAddress = getCompleteAddressString(double_lat, double_lng);
                Log.d("===", double_lat + "  " + double_lng);
//                    String resultAddress = getCompleteAddressString(16.43, 102.8);

                result.setText(resultAddress);

            } else if (radio_addr.isChecked()) {

                String address = addr.getText().toString();
                getLocationFromAddress(address);

            }

        }

        if(v == radio_addr || v == radio_lat_lng){
            checkFunction();
        }
    }
}
